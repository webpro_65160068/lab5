type Gender = 'male'|'female'|'others'
type Role = 'admin'|'user'
type User = {
    id: number
    email: string
    password: string
    fullName: string
    gender: Gender
    roles: Role[]
}

export type {Gender,Role,User}